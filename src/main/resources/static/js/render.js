'use strict';

function renderBeerList() {
    fetch('/beers', {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(beers => {
        let template = document.getElementById('template').innerHTML;
        let rendered = Mustache.render(template, beers);
        document.getElementById('beerlist').innerHTML = rendered;
        });
}