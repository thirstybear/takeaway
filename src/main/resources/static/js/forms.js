'use strict';

function showErrorMessage() {
    console.log('+++ TURN ON ERROR CODE!!! +++');
    document.getElementById("errormsg").style.visibility = "visible";
}

function addABeer() {
    let form = document.querySelector('form');
    let formData = new FormData(form);
    if (isNaN(formData.get('abv'))) {
        console.error("Error: ABV number format", formData.get('abv'))
        return false; // safe to do this
    }
    formData.delete('submit');

    let beer = JSON.stringify(Object.fromEntries(formData));
    StockService.add(beer).then(submittedOk => {
        if(submittedOk) {
            form.reset();
        } else {
            showErrorMessage();
        }
    });

    return false;
}

const StockService = {
    async add(item) {
        try {
            let response = await fetch('/beers', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: item
            });

            return response.ok;
        } catch (e) {
            return false;
        }
    }
}

// TODO Break out StockService to a separate object & combine with function in render.js
// TODO Add constructor injection to StockService so it can point to wherever (testability)
// TODO Retrofit Testing. This is currently a bit hacky while working out new async stuff.

