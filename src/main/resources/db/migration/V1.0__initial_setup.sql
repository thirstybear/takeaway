create table beer
(
    id bigserial primary key,
    name varchar(20),
    brewery varchar(20),
    abv float
);