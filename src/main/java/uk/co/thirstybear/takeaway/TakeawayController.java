package uk.co.thirstybear.takeaway;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping(TakeawayController.BASE_PATH)
public class TakeawayController {

  private final BeerStockService beerStockService;
  final static String BASE_PATH = "/beers";

  public TakeawayController(BeerStockService beerStockService) {
    this.beerStockService = beerStockService;
  }

  @GetMapping
  public StockList getStockList() {
    return new StockList(beerStockService.getBeerList());
  }

  @PostMapping
  public Beer addBeer(@RequestBody Beer beer, HttpServletResponse response) {
    System.out.println("Adding: " + beer);
    Beer savedBeer = addBeer(beer);
    response.addHeader("Location", String.format("%s/%s", BASE_PATH, savedBeer.getId()));
    response.setStatus(CREATED.value());
    return savedBeer;
  }

  Beer addBeer(@RequestBody Beer beer) {
    return beerStockService.addABeerToStock(beer);
  }
}
