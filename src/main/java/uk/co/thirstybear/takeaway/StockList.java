package uk.co.thirstybear.takeaway;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Collections;
import java.util.List;

import static java.util.Collections.unmodifiableList;

public class StockList {
  public final List<Beer> beers;

  @JsonCreator
  public StockList(List<Beer> beers) {
    this.beers = unmodifiableList(beers);
  }
}
