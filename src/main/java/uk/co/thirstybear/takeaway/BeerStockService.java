package uk.co.thirstybear.takeaway;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BeerStockService {
  private final StockRepository stockRepository;

  public BeerStockService(StockRepository stockRepository) {
    this.stockRepository = stockRepository;
  }

  public List<Beer> getBeerList() {
    Iterable<Beer> beerIterable = stockRepository.findAll();
    List<Beer> beers = new ArrayList<>();
    beerIterable.forEach(beers::add);
    return beers;
  }

  public Beer addABeerToStock(Beer aBeer) {
    return stockRepository.save(aBeer);
  }
}
