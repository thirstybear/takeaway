package uk.co.thirstybear.takeaway;

import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class Beer {
  private String name;
  private String brewery;
  private double abv;

  @Id
  @GeneratedValue(strategy = IDENTITY)
  private Long id;

  protected Beer() {
    // required by JPA
  }

  protected Beer(Long id, String name, String brewery, double abv) {
    this.id = id;
    this.name = name;
    this.brewery = brewery;
    this.abv = abv;
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getBrewery() {
    return brewery;
  }

  public double getAbv() {
    return abv;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Beer beer = (Beer) o;
    return Double.compare(beer.abv, abv) == 0 &&
        Objects.equals(name, beer.name) &&
        Objects.equals(brewery, beer.brewery) &&
        Objects.equals(id, beer.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, brewery, abv, id);
  }

  @Override
  public String toString() {
    return "Beer{" +
        "name='" + name + '\'' +
        ", brewery='" + brewery + '\'' +
        ", abv=" + abv +
        ", id=" + id +
        '}';
  }
}
