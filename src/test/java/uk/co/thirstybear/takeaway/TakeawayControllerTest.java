package uk.co.thirstybear.takeaway;

import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static uk.co.thirstybear.takeaway.BeerBuilder.ADNAMS_BEST;
import static uk.co.thirstybear.takeaway.BeerBuilder.ADNAMS_BEST_SAVED;

public class TakeawayControllerTest {
  @Test
  public void returnsEmptyListWhenNoBeerAvailable() {
    BeerStockService mockBeerStockService = mock(BeerStockService.class);

    assertThat(new TakeawayController(mockBeerStockService).getStockList().beers, is(emptyList()));
  }

  @Test
  public void readsListOfAvailableBeers() {
    List<Beer> beerList = List.of(ADNAMS_BEST);
    BeerStockService mockBeerStockService = mock(BeerStockService.class);
    when(mockBeerStockService.getBeerList()).thenReturn(beerList);

    assertThat(new TakeawayController(mockBeerStockService).getStockList().beers, is(beerList));
  }

  @Test
  public void addsBeerToDatabase() {
    BeerStockService mockBeerStockService = mock(BeerStockService.class);

    when(mockBeerStockService.addABeerToStock(ADNAMS_BEST)).thenReturn(ADNAMS_BEST_SAVED);

    assertThat(new TakeawayController(mockBeerStockService).addBeer(ADNAMS_BEST), is(ADNAMS_BEST_SAVED));
  }


}
