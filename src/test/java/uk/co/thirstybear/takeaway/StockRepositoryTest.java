package uk.co.thirstybear.takeaway;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsIterableContaining.hasItem;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;
import static uk.co.thirstybear.takeaway.BeerBuilder.ADNAMS_BEST;
import static uk.co.thirstybear.takeaway.BeerBuilder.ADNAMS_BEST_SAVED;

@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
@ContextConfiguration(initializers = {StockRepositoryTest.Initializer.class})
@Testcontainers
public class StockRepositoryTest {
  @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") // Intellij falsely flagging this
  @Autowired
  private StockRepository stockRepo;

  @Container
  private static final PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:12-alpine")
      .withDatabaseName("stockdb")
      .withUsername("app")
      .withPassword("password");

  @Test
  public void writesToRepo() {
    stockRepo.save(ADNAMS_BEST);

    Iterable<Beer> beerList = stockRepo.findAll();
    assertThat(beerList, hasItem(ADNAMS_BEST_SAVED));
  }

  static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
      TestPropertyValues.of(
          "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
          "spring.datasource.username=" + postgreSQLContainer.getUsername(),
          "spring.datasource.password=" + postgreSQLContainer.getPassword()
      ).applyTo(applicationContext.getEnvironment());
    }
  }
}