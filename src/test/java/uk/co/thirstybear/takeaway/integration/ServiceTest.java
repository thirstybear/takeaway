package uk.co.thirstybear.takeaway.integration;

import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import uk.co.thirstybear.takeaway.Beer;
import uk.co.thirstybear.takeaway.StockList;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static uk.co.thirstybear.takeaway.BeerBuilder.ADNAMS_BEST;

/*
Basic "kick the tyres" check on wiring.
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {ServiceTest.Initializer.class})
@Testcontainers
public class ServiceTest {
  @Container
  private static final PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:12-alpine")
      .withDatabaseName("stockdb")
      .withUsername("app")
      .withPassword("password");

  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  public void canAddAndView() {
    systemStartsEmpty();
    addABeer();
    thereIsABeerInTheSystem();
  }

  private void thereIsABeerInTheSystem() {
    ResponseEntity<StockList> response = restTemplate.exchange("/beers",
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<>() {}
    );
    assertThat(response.getBody().beers, hasSize(1));
  }

  private void addABeer() {
    ResponseEntity<Beer> response = restTemplate.postForEntity("/beers", ADNAMS_BEST, Beer.class);
    assertThat(response.getStatusCodeValue(), is(201));
    assertThat(response.getHeaders().get("Location").get(0), equalTo("/beers/1"));
    assertThat(response.getBody().getId(), equalTo(1L));
  }

  private void systemStartsEmpty() {
    ResponseEntity<StockList> response = restTemplate.exchange("/beers",
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<>() {}
        );
    assertThat(response.getBody().beers, is(empty()));
  }


  static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
      TestPropertyValues.of(
          "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
          "spring.datasource.username=" + postgreSQLContainer.getUsername(),
          "spring.datasource.password=" + postgreSQLContainer.getPassword()
      ).applyTo(applicationContext.getEnvironment());
    }
  }
}
