package uk.co.thirstybear.takeaway;

import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.Test;

import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static uk.co.thirstybear.takeaway.BeerBuilder.ADNAMS_BEST;

class BeerStockServiceTest {
  @Test
  public void delegatesToDedicatedRepoObject() {
    StockRepository mockRepository = mock(StockRepository.class);
    when(mockRepository.findAll()).thenReturn(singletonList(ADNAMS_BEST));

    BeerStockService beerStockService = new BeerStockService(mockRepository);

    assertThat(beerStockService.getBeerList(), IsIterableContaining.hasItem(ADNAMS_BEST));
    verify(mockRepository, times(1)).findAll();
  }

  @Test
  public void addingNewReturnsStoredBeerDetails() {
    StockRepository mockRepository = mock(StockRepository.class);
    when(mockRepository.save(ADNAMS_BEST)).thenReturn(ADNAMS_BEST);

    BeerStockService beerStockService = new BeerStockService(mockRepository);

    assertThat(beerStockService.addABeerToStock(ADNAMS_BEST), is(ADNAMS_BEST));
  }


}