package uk.co.thirstybear.takeaway;

import org.testcontainers.shaded.com.fasterxml.jackson.annotation.JacksonInject;

public class BeerBuilder {
  public final static Beer ADNAMS_BEST = aBeer()
      .fromBrewery("Adnams")
      .withName("Best Bitter")
      .withAbv(4.1)
      .build();

  public final static Beer ADNAMS_BEST_SAVED = aBeerLike(ADNAMS_BEST)
      .withId(1L)
      .build();

  private String brewery;
  private String beerName;
  private double abv;
  private long id;

  public static BeerBuilder aBeer() {
    return new BeerBuilder();
  }

  public static BeerBuilder aBeerLike(Beer beer) {
    return new BeerBuilder()
        .withName(beer.getName())
        .fromBrewery(beer.getBrewery())
        .withAbv(beer.getAbv());
  }

  public Beer build() {
    return new Beer(id, beerName, brewery, abv);
  }

  public BeerBuilder fromBrewery(String brewery) {
    this.brewery = brewery;
    return this;
  }

  public BeerBuilder withName(String beerName) {
    this.beerName = beerName;
    return this;
  }

  public BeerBuilder withAbv(double abv) {
    this.abv = abv;
    return this;
  }

  public BeerBuilder withId(long id) {
    this.id = id;
    return this;
  }
}
