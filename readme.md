# Takeaways

A simple online service to allow a small, usually shop based retailer to list items for pickup/takeaway
during the Covid-19 crisis. Main end customer: my local pub! 🙂🍺

## Parameters
| Parameter         | Detail          |
|-------------------|-----------------|
| POSTGRES_HOST     | DB host machine |
| POSTGRES_USER     | app db username |
| POSTGRES_PASSWORD | app db passwd   |

## Requirements

As a pub local 
I want to be able to see what beers are available
So that I can phone up and ask for a takeaway to pick up

As a Virtual BarTender
I want to be able to change the list of beers available
So that people do not ask for anything not available

As an Admin
I want to be able to add and delete BarTenders
So that I do not have to keep the stock updated myself 